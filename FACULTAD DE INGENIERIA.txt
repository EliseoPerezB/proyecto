FACULTAD_DE_INGENIERÍA                              CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Ingeniería_Ambiental 								19074	 10	  30   10    40   -    10  500   500   740.70 547.70      55   4
Ingeniería_Civil 									19021	 10	  20   15    45   10o  10  500   500   659.10 523.05      90   3
Ingeniería_Civil_Ambiental 							19018	 10	  20   10    45   -    15  500   500   S/I Carrera nueva  35   2
Ingeniería_Civil_Biomédica 							19076	 10	  30   10    40   10o  10  500   500   749.80 584.00      60   2
Ingeniería_Civil_Industrial_-_Valparaíso 			19052	 10	  30   10    40   10o  10  500   500   690.70 582.20      110  10
Ingeniería_Civil_Industrial_-_Santiago 				19094	 10	  30   10    40   10o  10  500   500   691.00 563.10      90   10
Ingeniería_Civil_Informática 						19085	 10	  30   10    40   10o  10  500   500   651.70 541.60      75   5
Ingeniería_Civil_Matemática 						19019	 10	  20   10    45   -    15  500   500   S/I Carrera nueva  15   2
Ingeniería_Civil_Oceánica 							19081	 10	  40   10    30   10o  10  500   475   626.60 501.20      30   2
Ingeniería_en_Construcción 							19026	 10	  25   10    45   10o  10  500   475   674.55 544.10      80   5