#include "facultad.h"
#include <stdio.h>
#include <stdlib.h>
void cargarTitulo(char path[], enunciados postulaciones[]){//crea el archivo con la informacion de la facultad
	FILE *facul;
	int i = 0;
	if((facul=fopen(path,"r"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0); 
	}
	else{
		printf("\ninformacion de las facultades: \n");
		while (feof(facul) == 0) {//crea el archivo hasta que no encuentre un caracter
			fscanf(facul,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s", postulaciones[i].facultad, postulaciones[i].codigo, postulaciones[i].nem, postulaciones[i].rank, postulaciones[i].leng, postulaciones[i].mat, postulaciones[i].hist, postulaciones[i].cs, postulaciones[i].pond, postulaciones[i].psu, postulaciones[i].max, postulaciones[i].min, postulaciones[i].cupsu, postulaciones[i].bae);
			printf("%s \t%s %s %s %s %s %s %s %s %s %s %s %s %s \n", postulaciones[i].facultad,postulaciones[i].codigo, postulaciones[i].nem, postulaciones[i].rank, postulaciones[i].leng, postulaciones[i].mat, postulaciones[i].hist, postulaciones[i].cs, postulaciones[i].pond, postulaciones[i].psu, postulaciones[i].max, postulaciones[i].min, postulaciones[i].cupsu, postulaciones[i].bae);
			i++;
			}
		}
		fclose(facul);
}


void lista(char path[], enunciados postulaciones[]){//lee sin imprimir, la cual es util para la funcion 1
	FILE *file;
	int i = 0;
	if((file=fopen(path,"r"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0); 
	}
	else{
		printf("\ninformacion de las facultades: \n");
		while (feof(file) == 0) {
			fscanf(file,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s", postulaciones[i].facultad, postulaciones[i].codigo, postulaciones[i].nem, postulaciones[i].rank, postulaciones[i].leng, postulaciones[i].mat, postulaciones[i].hist, postulaciones[i].cs, postulaciones[i].pond, postulaciones[i].psu, postulaciones[i].max, postulaciones[i].min, postulaciones[i].cupsu, postulaciones[i].bae);
			i++;
			}
		}
		fclose(file);
}

/*void grabarEstudiantes(char path[], estudiante curso[]){
	FILE *file;
	if((file=fopen(path,"w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		for (int i = 0; i<24; i++)
			fprintf(file,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].asig_1.proy1, curso[i].asig_1.proy2, curso[i].asig_1.proy3,
 curso[i].asig_1.cont1, curso[i].asig_1.cont2, curso[i].asig_1.cont3, curso[i].asig_1.cont4, curso[i].asig_1.cont5, curso[i].asig_1.cont6, curso[i].prom);
		
	} 
	printf("\n **El registro de estudiantes fue almacenado de manera correcta**\n \n ");
	fclose(file);
}

*/