CARRERAS

FACULTAD_DE_ARQUITECTURA 							CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA	
Arquitectura 										19020	 10  40    20	 20	  10   -   500   475   728.80 569.90      120  3
Cine 												19024	 10  40    30    10   10o 10   500   475   762.40 572.90      40   5
Diseno 												19022	 10	 40    20    20   10o 10   500   475   692.10 504.60      75   4
Gestio_en_Turismo_y_Cultura 						19027	 10	 40    30    10   10o 10   500   475   721.70 549.10      40   5
Teatro_(requiere_prueba_especial) 					19028	 10	 20    40    10   20   -   500   475   762.76 574.92      20   5

FACULTAD_DE_Ciencias                                CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Ingeniería_en_Estadística 							19072	 10	  40   10    30   10o  10  500   475   671.80 506.80      30   5
Lic._en_Ciencias_mención:_Biología_o_Química 		19075 	 10	  40   20    20   -    10  500   475   735.50 538.10      25   2
Lic._en_Física mención: Astronomía_Ciencias_Atmosféricas_o_Computación_Científica	19078	 10	  20   15    45   -    10  500   500   659.65 510.00      45   5
Licenciatura_en_Matemáticas 						19070	 10	  40   10    30   10o  10  500   475   632.20 506.10      15   2

FACULTAD_DE_CIENCIAS_DEL_MAR_Y_DE_RECURSOS_NATURALES  CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Biología_Marina 									  19080	   10  40    15    25   -    10  500   475   691.35 502.45    50   5

FACULTAD_DE_CIENCIAS_ECONÓMICAS_Y_ADMINISTRATIVAS   CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Administración_Hotelera_y_Gastronómica 				19086	 10	  40   20    20   10o  10  500   475   731.60 500.80      60   5
Administración_Pública_-_Valparaíso 				19068	 10	  20   35    25   10   -   500   500   723.45 574.90      75   2
Administración_Pública_-_Santiago 					19005	 10	  20   35    25   10   -   500   500   655.60 571.45      75   2
Auditoría_-_Valparaíso 								19062	 10	  40   20    15   15o  15  500   475   725.45 509.65      100  5
Auditoría_-_Santiago 								19093	 10	  40   20    15   15o  15  500   475   729.65 500.55      60   5
Auditoría_Vespertino_-_Valparaíso 					19069	 10	  40   20    15   15o  15  500   475   700.15 504.55      20   0
Ingeniería_Comercial_-_Viña del Mar 				19060	 10	  40   10    30   10   -   500   500   705.20 578.10      150  12
Ingeniería_Comercial - Santiago 					19095	 10	  40   10    30   10   -   500   500   647.30 500.90      125  12
Ingeniería_en_Información_y_Control_de_Gestión  	19083	 10	  40   10    30   10o  10  500   475   722.80 501.80      50   5
Ingeniería_en_Negocios_Internacionales_-_Viña 		19088	 10	  30   20    30   10o  10  500   475   693.60 529.20      80   5
Ingeniería_en_Negocios_Internacionales_-_Stgo.  	19089	 10	  30   20    30   10o  10  500   475   694.70 511.80      35   5

FACULTAD_DE_DERECHO_Y_CIENCIAS_SOCIALES             CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Derecho 											19030	 10	  25   20    20   25   -   500   500   737.30 628.90      135  5
Trabajo_Social 										19031	 20	  30   20    20   10   -   500   475   700.00 556.40      78   2
	
FACULTAD_DE_FARMACIA                                CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Química_y_Farmacia 									19090	 10	  40   20    20   -    10  500   500   760.80 620.50      60   2
Nutrición_y_Dietética 								19091	 10	  40   20    20   -    10  500   500   770.40 608.90      60   2
	
FACULTAD_DE_HUMANIDADES                             CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Pedagogía_en_Filosofía 								19010	 10	  40   20    10   20   -   500   500   718.90 505.70      30   2
Pedagogía_en_Historia_y_Ciencias_Sociales 			19011	 10	  40   20    10   20   -   500   500   741.90 552.60      70   3
Pedagogía_en_Música 								19015	 10	  40   25    15   10   -   500   500   619.60 529.95      40   2
Sociología 											19012	 10	  40   20    15   15   -   500   475   736.35 546.05      75   2
  
FACULTAD_DE_INGENIERÍA                              CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Ingeniería_Ambiental 								19074	 10	  30   10    40   -    10  500   500   740.70 547.70      55   4
Ingeniería_Civil 									19021	 10	  20   15    45   10o  10  500   500   659.10 523.05      90   3
Ingeniería_Civil_Ambiental 							19018	 10	  20   10    45   -    15  500   500   S/I Carrera nueva  35   2
Ingeniería_Civil_Biomédica 							19076	 10	  30   10    40   10o  10  500   500   749.80 584.00      60   2
Ingeniería_Civil_Industrial_-_Valparaíso 			19052	 10	  30   10    40   10o  10  500   500   690.70 582.20      110  10
Ingeniería_Civil_Industrial_-_Santiago 				19094	 10	  30   10    40   10o  10  500   500   691.00 563.10      90   10
Ingeniería_Civil_Informática 						19085	 10	  30   10    40   10o  10  500   500   651.70 541.60      75   5
Ingeniería_Civil_Matemática 						19019	 10	  20   10    45   -    15  500   500   S/I Carrera nueva  15   2
Ingeniería_Civil_Oceánica 							19081	 10	  40   10    30   10o  10  500   475   626.60 501.20      30   2
Ingeniería_en_Construcción 							19026	 10	  25   10    45   10o  10  500   475   674.55 544.10      80   5
	
FACULTAD_DE_MEDICINA                                CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Educación_Parvularia 								19037	 10	  40   30    10   10o  10  500   500   652.10 530.20      35   2
Enfermería_-_Reñaca 								19041	 10	  40   25    15   -    10  500   500   773.10 681.40      75   3
Enfermería_-_San_Felipe 							19042	 10	  40   25    15   -    10  500   500   729.85 645.80      40   2
Fonoaudiología_-_Reñaca 							19046	 10	  40   30    10   -    10  500   500   729.70 633.70      45   2
Fonoaudiología_-_San Felipe 						19044	 10	  40   30    10   -    10  500   500   664.60 566.10      40   2
Kinesiología 										19043	 10	  30   20    20   -    20  500   500   745.95 602.60      80   2
Medicina_-_Reñaca 									19040	 10	  30   20    20   -    20  600   600   815.90 762.50      72   2
Medicina_-_San Felipe 								19039	 10	  30   20    20   -    20  600   600   764.10 751.30      42   2
Obstetricia_y_Puericultura_-_Reñaca 				19047	 10	  40   30    10   -    10  500   500   778.10 691.10      75   2
Obstetricia_y_Puericultura_-_San_Felipe 			19036	 10	  40   30    10   -    10  500   500   723.50 670.20      25   2
Psicología 											19045	 10	  30   30    20   10   -   500   500   700.20 623.80      80   3
Tecnología_Médica_-_Reñaca 							19049	 10	  40   25    15   -    10  500   500   756.00 697.20      55   2
Tecnología_Médica_-_San Felipe 						19048	 10	  40   25    15   -    10  500   500   819.15 649.10      40   2
	
FACULTAD_DE_ODONTOLOGIA								CODIGO	NEM	RANK	LENG	MAT	HIST	CS	POND	PSU	MAX	MIN	PSU	BEA
Odontologia											19050	10	40	20	20	-	10	500	500	762.10	690.50	72	2	