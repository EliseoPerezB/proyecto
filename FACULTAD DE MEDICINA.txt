FACULTAD_DE_MEDICINA                                CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Educación_Parvularia 								19037	 10	  40   30    10   10o  10  500   500   652.10 530.20      35   2
Enfermería_-_Reñaca 								19041	 10	  40   25    15   -    10  500   500   773.10 681.40      75   3
Enfermería_-_San_Felipe 							19042	 10	  40   25    15   -    10  500   500   729.85 645.80      40   2
Fonoaudiología_-_Reñaca 							19046	 10	  40   30    10   -    10  500   500   729.70 633.70      45   2
Fonoaudiología_-_San Felipe 						19044	 10	  40   30    10   -    10  500   500   664.60 566.10      40   2
Kinesiología 										19043	 10	  30   20    20   -    20  500   500   745.95 602.60      80   2
Medicina_-_Reñaca 									19040	 10	  30   20    20   -    20  600   600   815.90 762.50      72   2
Medicina_-_San Felipe 								19039	 10	  30   20    20   -    20  600   600   764.10 751.30      42   2
Obstetricia_y_Puericultura_-_Reñaca 				19047	 10	  40   30    10   -    10  500   500   778.10 691.10      75   2
Obstetricia_y_Puericultura_-_San_Felipe 			19036	 10	  40   30    10   -    10  500   500   723.50 670.20      25   2
Psicología 											19045	 10	  30   30    20   10   -   500   500   700.20 623.80      80   3
Tecnología_Médica_-_Reñaca 							19049	 10	  40   25    15   -    10  500   500   756.00 697.20      55   2
Tecnología_Médica_-_San Felipe 						19048	 10	  40   25    15   -    10  500   500   819.15 649.10      40   2